import React, { Component } from "react";
import Link from "gatsby-link";
import "./App.css";
import bb from "./bombersbar.json";
import stranger from "./stranger.json";
import "./uikit/css/uikit.min.css";
import ReactDOMServer from "react-dom/server";

const handleClick = (i, event) => {
  if (i !== 12) {
    console.log(event.currentTarget.style.background);
    switch (event.currentTarget.style.background) {
      case "rgb(41, 50, 60) none repeat scroll 0% 0%":
        event.currentTarget.style.background = "#fff";
        event.currentTarget.getElementsByTagName("h3")[0].style.color = "#333"
        break;
      case "rgb(41, 50, 60)":
        event.currentTarget.style.background = "#fff";
        event.currentTarget.getElementsByTagName("h3")[0].style.color = "#333"
        break;
      default:
        event.currentTarget.style.background = "#29323c";
        event.currentTarget.getElementsByTagName("h3")[0].style.color = "#fff"
        break;
    }
  }
};

function shuffle(array) {
  for (var i = array.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}



var json = stranger;
class IndexPage extends Component {
  componentDidMount() {
    var grid = document.getElementsByClassName("bingo")[0];
    grid.setAttribute("uk-grid", "true");
    document.title = "Stranger Danger Bingo";
  }

  switchFile(event) {
    if (event.currentTarget.innerText === "BOMBERS BAR") {
      json = bb;
      event.currentTarget.innerText = "Stranger";
      window.document.title = "Bombers Bar Bingo";
      this.forceUpdate();
    } else {
      json = stranger;
      event.currentTarget.innerText = "BOMBERS BAR";
      window.document.title = "Stranger Danger Bingo";
      this.forceUpdate();
    }
  }


  render() {
    const dynHtml = shuffle(json.items).map(function(item, i) 
    {
      if(i < 25)
      return (<div
            className="uk-card uk-card-default uk-width-1-5@m"
            onClick={handleClick.bind(this, i)}
            id="card"
          >
            <div className="uk-card-header">
              <div className="uk-grid-small uk-flex-middle">
                <div className="uk-width-auto" />
                <div className="uk-width-expand">
                  <h3 className="uk-card-title uk-margin-remove-bottom" key={item}>
                    {i != 12 && item.title} {i == 12}
                  </h3>
                </div>
              </div>
            </div>
          </div>
    );
  }
    );






    return (
      <div>
        <div className="menu uk-section uk-section-xsmall uk-section-primary uk-flex uk-flex-center">
          <button
            className="uk-button uk-button-default uk-float-left"
            type="button"
            aria-expanded="false"
            onClick={this.switchFile.bind(this)}
          >
            Bombers Bar
          </button>
        </div>
        <div className="uk-section uk-section-large uk-section-primary uk-preserve-color">
          <div className="window">
            <div
              className="bingo uk-child-width-1-5@s uk-grid-match"
              ref={ref => (this.grid = ref)}
            >
              {dynHtml}

            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default IndexPage;
