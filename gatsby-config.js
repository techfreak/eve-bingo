module.exports = {
  pathPrefix: `/eve-bingo`,
  siteMetadata: {
    title: 'Gatsby Default Starter',
  },
  plugins: ['gatsby-plugin-react-helmet'],
};
